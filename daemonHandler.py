# Ben Targan

# 1 and only 1 instance of alprd must be running before this script is executed
# modify configs to set up alprd camera stream at config/alprd.conf.defaults

# use beanstalkc to drain beanstalkd queue
import beanstalkc

# for interfacing with mySQL
import mysql.connector as mysql
from mysql.connector import Error

# stdlib imports
import sys
# for grabbing the correct image from folder
import glob
import ConfigParser

# sends jpgData to an s3 bucket
# def sendToBucket(jpgData, unixTime):
# TODO: setup bucket, send image to bucket, delete local image once sent


# sends paramaters to db
def insertIntoDB(db, table, LP, confidenceVal, unixTime, siteName, cameraID):
    command = "INSERT INTO " + table + "(LP,confidenceVal,unixTime,siteName,cameraID) VALUES(%s,%s,%s,%s,%s)"
    vals = (LP, confidenceVal, unixTime, siteName, cameraID)

    cursor = db.cursor()
    cursor.execute(command, vals)
    db.commit()
    cursor.close()

    print("sent\t" + LP + "\t" + unixTime)


# grabs jpg from platesDir that matches unixTime (unneeded until bucket is set up)
# def grabJPGAt(unixTime):
#     platesDir = ''
#     name = platesDir + '*' + unixTime + '.jpg'
#     filename = glob.glob(name)[0]
#     with open(filename, 'rb') as f:
#         photo = f.read()
#     return photo


# grabs job from beanstalk, processes string and sends to db
def consumeAndProcessJob(beanstalk, db, table):
    # DEBUG: for flushing queue if desynced with platesDir
    # while True:
    #     j = beanstalk.reserve()
    #     j.delete()
    #     print("j deleted")

    while True:
        # allows script to timeout and cleanly disconnect from db
        j = None
        j = beanstalk.reserve(timeout=10)
        if j is None:
            print('timing out...')
            break

        jobString = str(j.body)

        # split the unix time out of the job
        unixTime = jobString.split('"uuid":', 1)[1]
        unixTime = unixTime.split(',',1)[0]
        unixTime = unixTime[-14:-1]

        # split the site name out of the job
        siteName = jobString.split('"site_id":',1)[1]
        siteName = siteName.split(',',1)[0]
        siteName = siteName.strip('"')

        cameraID = jobString.split('"camera_id"',1)[1]
        cameraID = cameraID.split(',',1)[0]
        cameraID = cameraID.strip(":")


        candidates = jobString.split("candidates")

        for entry in candidates:
            # split the plate and confidence value out of each entry in the job, discard the rest
            entry = entry.split('plate', 1)[1]
            entry = entry.split('matches_template', 1)[0]
            entry = entry.strip()
            entry = entry.replace('"', '')
            entry = entry.replace(':', '')
            entry = entry.replace(',', ' ')
            entry = entry.replace('confidence', '')
            LP = entry.split(' ')[0]
            confidenceVal = entry.split(' ')[1]

            # TODO: grab jpg file data using unix time and send to bucket
            # jpgData = grabJPGAt(unixTime)
            # sendToBucket(jpgData, unixTime)

            insertIntoDB(db, table, LP, confidenceVal, unixTime, siteName, cameraID)

        j.delete()


def main():
    # reading from config file
    configFile = 'pyconfig.ini'
    config = ConfigParser.ConfigParser()
    config.read(configFile)

    beanHost = config.get('beanstalkc', 'host')
    beanPort = int(config.get('beanstalkc', 'port'))
    beanTube = config.get('beanstalkc', 'tube')

    sqlHost = config.get('mySQL Connector', 'host')
    sqlUser = config.get('mySQL Connector', 'user')
    sqlPasswd = config.get('mySQL Connector', 'passwd')
    sqlDb = config.get('mySQL Connector', 'db')
    sqlTable = config.get('mySQL Connector', 'table')

    # beanstalkd setup
    print('setting up beanstalkc')
    beanstalk = beanstalkc.Connection(beanHost, beanPort)
    print('using tube: ' + beanTube)
    beanstalk.watch(beanTube)

    # mySQL setup
    print('connecting to mySQL...')
    db = mysql.connect(
        host=sqlHost,  
        user=sqlUser,
        passwd=sqlPasswd,
        db=sqlDb)
    if db.is_connected():
        print("done")

    print('starting consumer loop')
    consumeAndProcessJob(beanstalk, db, sqlTable)
    print('end consumer loop')

    print('disconnecting from mySql db')
    db.close()


if __name__ == '__main__':
    main()
