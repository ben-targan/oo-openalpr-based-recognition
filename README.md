#Readme

##About
Using [OpenALPR][1], this python code allows you to automatically extract license plates from a camera stream(s) and send them to a mySQL database.  OpenALPR processes the video and outputs the information using a beanstalkd queue.  The output from openALPR is formatted down to only the most relevant plate number, its confidence value, and its Unix timestamp per detected plate per frame of video.  A JPG from OpenALPR is stored locally. This repo also contains a Ruby on Rails based web interface for querying the mySQL server and displaying the results.

This code was written on / designed for `Ubuntu 16.04`.

The Rails site was built based on [this guide][5].

---

##Python
* **`daemonHandler.py`** is used to process the output of `alprd` (OpenALPR's built in daemon). It is what we are using to take the outputted license plates and send them to a mySQL database.  

---

##Ruby on Rails based Web Interface
* The web interface for accessing the mySQL database is located at `rails/webInterface`

* Database
    * this app uses an existing mySQL database.
    * this code assumes that your database has a table named `entries`.

* Services
    * displays information from a mySQL database
    * filters what is displayed, removing garbage data (does not group related plates, but removes data with less than 80% confidence)
    * allows for search based on LP or Date

---

##Prerequisites 

*  Install [openALPR][1] using `sudo apt-get update && sudo apt-get install -y openalpr openalpr-daemon openalpr-utils libopenalpr-dev`

* In order to interact with the output from `alprd`, you will need beanstalkd. It can be installed with `sudo apt install beanstalkd`

* **Python**
    * `sudo apt install python2.7 python-pip`

    * You will need [beanstalkc][3] (a Python client library for beanstalkd), for draining openALPR's beanstalkd queue with Python.  Clone the repo and install with `python setup.py install`
        * beanstalkc depends on `pyYAML`, but it is not needed for our use.

    * Install [mySQL connector][4], you will need it to send data from Python to your mySQL database. It can be installed with `pip install mysql-connector-python`

* **mySQL**
    * Install mySQL and set up server

* **Ruby on Rails**
    * There is a helpful install guide [here][6].

    * `Ruby 2.5.1`

    * `Rails 5.2.0`

    * The `mysql2` gem, which can be installed with `gem install mysql2`
        * `sudo apt-get install libmysqlclient-dev` is requred for rails to compile the gem

---

##Configuration
* Connection settings for Python to the beanstalkd queue and the mySQL server can be edited in the `pyconfig.ini` file.  This file is not present in the repo, but `pyconfig.ini.dist` contains a template.

* By default, `aplrd` opens a beanstalkd queue on `localhost` at port `11300` and uses the `alprd` tube.

* `alprd` can be configured at `/etc/openalpr/alprd.conf`
    * The camera stream(s) that alprd uses can be configured with `stream = [local or IP mjpeg stream]`.  It can be either a local device or an IP camera stream. Additional streams can be declared on separate lines with the same syntax.  Each camera stream will have a separate ID, starting with 1, in the order they are listed.  Each camera stream will be processed on a separate processor core.

    * In order to upload images of the captured plates, they must be stored with `store_plates = 1`, and `store_plates_location = [your/directory]`
        * In order for the images need to be directly accessible from the rails website, they should be stored in `rails/webInterface/assets/images/`

    * By default, the images do not have a site name attached, to add a site name to the filename and insert it into the database, add `site_id = [your-name]`.  Note that the site name is for this instance of alprd.  All camera streams will have the same site name, but different ID's.  

    * By default, the maximum resolution supported is 1280 x 720, in order for alprd to use a higher resolution, it must be specified in the config file as `max_detection_input_height = [size]` and `max_detection_input_width = [size]`

* The mySQL database should contain a table called `entries`, with fields for: 
    * `LP`: license plate string
    * `confidenceVal`: confidence value float
    * `unixTime`: timestamp since epoch bigint
    * `siteName`: name for where the camera is located
    * `cameraID`: integer identifier 

* `rails/webInterface/config/database.yml` has been ignored from this repo.  You must replace it. There is a template called `database.yml.dist` in the same directory.


[1]: https://github.com/openalpr/openalpr "OpenALPR github"
[2]: https://github.com/openalpr/openalpr/wiki/OpenALPR-Daemon-(alprd) "OpenALPR daemon"
[3]: https://github.com/earl/beanstalkc/ "Python client library for beanstalkd"
[4]: https://dev.mysql.com/downloads/connector/python/8.0.html "mySQL Connector"
[5]: https://codeburst.io/how-to-build-a-rails-app-on-top-of-an-existing-database-baa3fe6384a0 "Ruby on Rails setup guide"
[6]: https://gorails.com/setup/ubuntu/16.04 "Ruby and Rails Installation guide"