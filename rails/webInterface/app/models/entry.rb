class Entry < ApplicationRecord
    # for filtering out lower confidence entries
    scope :above80, -> {where("confidenceVal >= 80")}

    # Search by License Plate
    def self.searchLP(searchLP)
        if searchLP == ""
            return where(nil)
        end
        searchLP = "%" + searchLP + "%"
        where("LP LIKE ?", searchLP)
    end

    # Search by Date
    def self.searchDate(searchDate) # YYYY-MM-DD
        #formatting input
        if searchDate == ""
            return where(nil)
        end
        formattedDate = DateTime.parse(searchDate.to_s)
        formattedDate = formattedDate.change(:offset => "-0700")
        formattedDate = formattedDate.change(:hour => 0).change(:min => 0).change(:sec => 0)

        timeSinceEpoch = formattedDate.to_datetime.strftime("%Q").to_i

        oneDayLater = (timeSinceEpoch + (1000 * 60 *60 * 24)).to_i

        where("unixTime >= ? AND unixTime <= ?", timeSinceEpoch, oneDayLater)
    end
end