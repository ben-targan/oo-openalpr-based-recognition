json.extract! entry, :id, :LP, :confidenceVal, :unixTime, :jpgLink, :created_at, :updated_at
json.url entry_url(entry, format: :json)
